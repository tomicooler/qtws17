import qbs

Project {

    SubProject {
        filePath: "lib/lib.qbs"
    }

    SubProject {
        filePath: "app/app.qbs"
    }

}
