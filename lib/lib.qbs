import qbs

DynamicLibrary {
    name: "qtws17plugin"

    cpp.cxxLanguageVersion: "c++11"

    files: [
        "qtws17plugin.h",
        "qtws17plugin.cpp",
        "ctimer.h",
        "ctimer.cpp",
    ]

    Depends { name: "Qt.core" }
    Depends { name: "Qt.qml" }

    Export {
        Depends { name: "cpp" }
        cpp.includePaths: [product.sourceDirectory]
    }

    Group {
        name: "The qmldir file"
        files: "qmldir"
        qbs.install: true
        qbs.installDir: "QtWS17"
    }
    Group {
        name: "The App itself"
        fileTagsFilter: product.type
        qbs.install: true
        qbs.installDir: "QtWS17"
    }
}
