#pragma once

#include <QObject>
#include <QTime>

class QTimer;

class CTimer : public QObject
{
  Q_OBJECT

  Q_PROPERTY(QTime time READ time WRITE setTime NOTIFY timeChanged)
  Q_PROPERTY(QTime pTime READ pTime WRITE setPTime NOTIFY pTimeChanged)

public:
  explicit CTimer(QObject *parent = nullptr);

  QTime time() const;
  QTime pTime() const;
  Q_INVOKABLE void startPresentation();

public slots:
  void setTime(QTime time);
  void setPTime(QTime pTime);

signals:
  void timeChanged(QTime time);
  void pTimeChanged(QTime pTime);

protected slots:
  void update();

private:
  QTime m_time;
  QTime m_pTime;
  QTimer *m_timer;
  bool m_presenting;
};
