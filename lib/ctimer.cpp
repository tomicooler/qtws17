#include "ctimer.h"

#include <QTimer>

CTimer::CTimer(QObject *parent)
  : QObject(parent),
    m_pTime(0, 0),
    m_timer(new QTimer(this)),
    m_presenting(false)
{
  m_timer->setInterval(800);
  connect(m_timer, &QTimer::timeout, this, &CTimer::update);
  m_timer->start();
}

QTime
CTimer::time() const
{
  return m_time;
}

QTime
CTimer::pTime() const
{
  return m_pTime;
}

void
CTimer::startPresentation()
{
  m_presenting = true;
}

void
CTimer::setTime(QTime time)
{
  if (m_time == time)
    return;

  m_time = time;
  emit timeChanged(m_time);
}

void
CTimer::setPTime(QTime pTime)
{
  if (m_pTime == pTime)
    return;

  m_pTime = pTime;
  emit pTimeChanged(m_pTime);
}

void
CTimer::update()
{
  setTime(QTime::currentTime());
  if (m_presenting) // I know this is not proper, just demo purposes :)
    setPTime(pTime().addMSecs(m_timer->interval()));
}
