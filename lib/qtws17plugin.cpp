#include "qtws17plugin.h"
#include "ctimer.h"

#include <qqml.h>

void QtWS17Plugin::registerTypes(const char *uri)
{
  qmlRegisterType< CTimer >(uri, 1, 0, "CTimer");
}
