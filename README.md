### What is this repository for? ###

* This repositoriy is a small presentation about our Qt World Summit 2017 attendance.
* The project itself is a small Qt app with a C++ Qml extension plugin, qbs project example.

### How do I get set up? ###

* Requirement: Qt 5.9 or later with qbs configured.
* Just press the Run button in QtCreator, it should work.