import qbs

CppApplication {
    name: "qtws17"

    cpp.cxxLanguageVersion: "c++11"

    // Additional import path used to resolve QML modules in Qt Creator's code model
    property pathList qmlImportPaths: [
        qbs.installRoot,
        sourceDirectory + "/qml"
    ]

    files: [
        "main.cpp",
    ]

    Depends { name: "Qt.core" }
    Depends { name: "Qt.quick" }
    Depends {
        name: "qtws17plugin"
        cpp.link: false
    }

    Group {
        name: "Runtime resources"
        files: [
            "**/*.qml",
            "**/qmldir",
            "qtquickcontrols2.conf",
            "**/*.jpg",
        ]
        qbs.installSourceBase: sourceDirectory
        qbs.install: true
    }

    Group {
        name: "The App itself"
        fileTagsFilter: "application"
        qbs.install: true
    }
}
