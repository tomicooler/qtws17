#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

int main(int argc, char *argv[])
{
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QGuiApplication app(argc, argv);

  qputenv("QT_QUICK_CONTROLS_CONF", QString("%1/%2").arg(app.applicationDirPath(), QLatin1String("qtquickcontrols2.conf")).toUtf8());

  QQmlApplicationEngine engine;
  engine.rootContext()->setContextProperty("appDir", app.applicationDirPath());
  engine.addImportPath(app.applicationDirPath() + "/qml");
  engine.load(QUrl(app.applicationDirPath() + "/qml/main.qml"));
  if (engine.rootObjects().isEmpty())
    return -1;

  return app.exec();
}
