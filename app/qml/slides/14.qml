import Components 1.0

Slide {
    title: "QStringView"
    content: [
        "QStringRef, but not only for QString",
        "UTF-16 only",
        "  QString, u\"literal\", wchar_t* + size,",
        "  std::basic_string<ushort>, QVector/std::vector<char16_t>...",
        "nearly all QString const api"
    ]
}
