import Components 1.0

Slide {
    title: "Other"
    content: [
        "First experience with Qt for mobile",
        "  highdpi :)",
        "Integrating out of process graphical content into a QtQuick scene",
        "  EGLStream",
        "  QtWayland.Compositor",
        "QtWebGl",
    ]
}
