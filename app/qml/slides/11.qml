import Components 1.0

Slide {
    title: "Technical deep dive track"
    content: [
        "Inside the Qt object model",
        "Multithreading with Qt",
        "QML for C++ developers",
        "QObject deep dive"
    ]
}
