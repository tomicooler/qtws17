import Components 1.0

DarkSlide {
    title: "Let's go home!"
    backgroundImageSource: Qt.resolvedUrl("pics/letsgohome.jpg")
}
