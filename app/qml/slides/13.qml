import Components 1.0

Slide {
    title: "Improve your productivity with Clang tools"
    content: [
        "static code analysis, Abstract Syntax Tree",
        "sanitizers: address, memory, undefined behaviour, thread",
        "clang-format",
        "clang-tidy (add override, modernize-loop, misleading indentation)",
        "clazy (for Qt)",
    ]
}
