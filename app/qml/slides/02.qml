import Components 1.0

Slide {
    title: "Keynotes"
    imageSource: Qt.resolvedUrl("pics/ws-1.jpg")
    content: [
        "Qt Roadmap",
        "  vulkan, 3D / Studio",
        "  connectivity",
        "  optimizations",
        "  Qt Widgets",
        "Modern C++",
        "  metaclasses",
        "Hello Ruby",
        "  www.helloruby.com",
        "ATLAS experiment",
    ]
}
