import Components 1.0

Slide {
    title: "cubes - qbs"
    content: [
        "qbs is an all-in-one tool that generates a build graph. (QML)",
        " Primitives",
        "  Artifacts: represents i/o files",
        "  File tags: classify artifacts into different types",
        "  Rules: input artifacts -> output artifacts",
        "  Modules: rules + properties",
        "  Dependencies: can be expressed between modules",
        "  Properties: can be applied to *",
        "NOT tied to Qt or even C++"
    ]
}
