import QtQuick 2.0
import QtQuick.Controls.Material 2.2
import QtQuick.Controls.Universal 2.2

Slide {
    Material.theme: Material.Light
    Universal.theme: Universal.Light
}
