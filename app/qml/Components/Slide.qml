import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Page {
    id: root

    property var content: []
    property url imageSource
    property url backgroundImageSource

    header: ToolBar {
        visible: title.length > 0
        RowLayout {
            anchors.fill: parent
            Label {
                text: root.title
                font.bold: true
                font.pixelSize: 32
                Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            }
        }
    }

    Image {
        anchors.fill: parent
        visible: status === Image.Ready
        sourceSize.height: root.height
        sourceSize.width: root.width
        smooth: true
        source: root.backgroundImageSource
        fillMode: Image.PreserveAspectFit
    }

    RowLayout {
        anchors.fill: parent
        anchors.margins: 30

        Label {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            visible: content.length === 1
            Layout.fillWidth: true
            text: content.length > 0 ? content[0] : ""
            font.pointSize: 48
            horizontalAlignment: Text.AlignHCenter
        }

        BulletPoints {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            visible: content.length > 1
            Layout.fillWidth: true
            content: root.content
        }

        Image {
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            visible: status === Image.Ready
            sourceSize.height: root.height / 1.3
            smooth: true
            source: root.imageSource
            fillMode: Image.PreserveAspectFit
        }
    }
}
