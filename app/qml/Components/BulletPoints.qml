import QtQuick 2.9
import QtQuick.Controls 2.2

Column {
    property var content: []

    spacing: 10

    Repeater {
        model: content.length

        Row {

            function decideIndentLevel(s) { return s.charAt(0) === " " ? 1 + decideIndentLevel(s.substring(1)) : 0 }
            property int maxIndentLevel: 3
            property int indentLevel: Math.min(decideIndentLevel(content[index]), maxIndentLevel)

            x: indentLevel * 10

            Rectangle {
                id: dot
                anchors.baseline: text.baseline
                anchors.baselineOffset: -text.font.pixelSize / 2
                width: text.font.pixelSize / 3
                height: text.font.pixelSize / 3
                color: text.color
                radius: width / 2
                opacity: text.text.length === 0 ? 0 : 1
            }

            Item {
                id: space
                width: dot.width * 1.5
                height: 1
            }

            Label {
                id: text
                text: content[index].trim()
                font.pointSize: 20 + (10 * (maxIndentLevel - indentLevel) / (maxIndentLevel))
            }
        }
    }
}
