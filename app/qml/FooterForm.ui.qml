import QtQuick 2.4
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.0

RowLayout {
    property alias currentTime: currentTime
    property alias presentationTime: presentationTime
    property alias pageCount: pageCount
    property alias pageNumber: pageNumber

    Label {
        id: presentationTime
        text: qsTr("00:02:59")
    }

    Item {
        id: spacer1
        Layout.fillWidth: true
    }

    Row {
        id: row1
        Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
        spacing: 3

        Label {
            id: pageNumber
            text: qsTr("1")
        }

        Label {
            id: label
            text: qsTr("/")
        }

        Label {
            id: pageCount
            text: qsTr("30")
        }
    }

    Item {
        id: spacer2
        Layout.fillWidth: true
    }

    Label {
        id: currentTime
        text: qsTr("13:55")
        Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
    }
}
