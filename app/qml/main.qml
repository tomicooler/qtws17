import QtQuick 2.7
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Window 2.3
import QtQuick.Controls.Material 2.2
import QtQuick.Controls.Universal 2.2
import Qt.labs.folderlistmodel 2.2

import QtWS17 1.0

ApplicationWindow {
    visible: true
    width: 600
    height: 400
    title: qsTr("Qt World Summit 2017")

    CTimer {
        id: ctimer
    }

    FolderListModel {
        id: slides
        sortField: FolderListModel.Name
        folder: appDir + "/qml/slides"
        showDirs: false
        nameFilters: ["*.qml"]
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent

        Repeater {
            model: slides
            Loader {
                active: SwipeView.isCurrentItem || SwipeView.isNextItem || SwipeView.isPreviousItem
                source: slides.folder + "/" + fileName
            }
        }
    }

    footer: Pane {
        Material.theme: Material.Dark
        Universal.theme: Universal.Dark

        Footer {
            anchors.fill: parent
            presentationTime.text: ctimer.pTime.toLocaleTimeString(Qt.locale(), "hh:mm:ss")
            pageCount.text: swipeView.count
            pageNumber.text: swipeView.currentIndex + 1
            currentTime.text: ctimer.time.toLocaleTimeString(Qt.locale(), "hh:mm:ss")
        }
    }

    Item {
        anchors.fill: parent
        focus: true

        Keys.onPressed: {
            event.accepted = true;

            switch (event.key) {
            case Qt.Key_F:
                visibility = visibility === Window.FullScreen ? Window.AutomaticVisibility : Window.FullScreen;
                break;
            case Qt.Key_S:
                ctimer.startPresentation();
                break;
            case Qt.Key_Left:
                if (swipeView.currentIndex > 0)
                    swipeView.currentIndex--;
                break;
            case Qt.Key_Right:
                if (swipeView.currentIndex < swipeView.count - 1)
                    swipeView.currentIndex++;
                break;
            default:
                event.accepted = false;
            }
        }
    }
}
